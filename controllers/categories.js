var express=require('express');
var router=express.Router();
var bodyParser=require('body-parser');
var bodyParserMid=bodyParser.urlencoded();
var fs=require('fs');
const Multer = require('multer');
const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
      fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
    }
  });
  var firebase = require('firebase-admin');

  var bucket = firebase.storage().bucket();
  // var path = require('path');
// var appDir = path.dirname(require.main.filename);
// console.log(appDir);

var db = firebase.database();
var ref= db.ref('categories');
var refAds= db.ref('ads');

var categoryRef=db.ref('categories');
router.get('/add',function (req,resp) {
    return resp.render('categories/add',{ msg:req.flash("msg")});        
});

    // var CategoryModel=mongoose.model('posts');

router.post('/add',multer.single('catImg'),function (req,resp) {
    var categorynameAr=req.body.nameAr;
    var categorynameEn=req.body.nameEn;
    let file=req.file;
    if(categorynameAr==""||categorynameEn==""){
        req.flash("msg","Category Feilds Is Required");
        return resp.redirect('add');
    }else{
        uploadImageToStorage(file).then((success) => {
            ref.push().set({
                "categoryNameEn":categorynameEn,
                "categoryNameAr":categorynameAr,
                "image":success
            });        
            }).
            then((success)=>{
                return resp.redirect('list');

            })
            .catch((error) => {
              console.error(error);
            });
      
    }
});
router.get('/ads',function (req,resp) {
    refAds.once("value", function(snapshot) {
        var result= snapshot.val();
        if(result){
            return resp.render('categories/ads',{imageUrl:result["image"],imageUrl1:result["image1"],imageUrl2:result["image2"], msg:req.flash("msg")});        
        }else{
            return resp.render('categories/ads',{imageUrl:{},imageUrl1:{},imageUrl2:{}, msg:req.flash("msg")});                    
        }
        }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });

});
router.post('/ads',multer.single('myAds'),function (req,resp) {
    let file=req.file;    
    var myImage=req.body.myImage;
    var key='';
    if(myImage){
        switch (parseInt(myImage)) {
            case 1:
                key="image";
                break;
                case 2:
                key="image1";
                break;
                case 3:
                key="image2"
                break;
        }
    }else{
        key="image";
    }
    if(!file){
        req.flash("msg","Category Feilds Is Required");
        return resp.redirect('ads');
    }else{
        uploadImageToStorage(file).then((success) => {
            refAds.once("value", function(snapshot) {
                        var result= snapshot.val();
                        result[key]=success;
                        refAds.set(result);
                    })        
            }).then(()=>{
                return resp.redirect("/users/list");
            })
            .catch((error) => {
              console.error(error);
            });
    }
});
//comment
/**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */
const uploadImageToStorage = (file) => {
    let prom = new Promise((resolve, reject) => {
      if (!file) {
        reject('No image file');
      }
      let myFile=`${file.originalname}_${Date.now()}`;
      let newFileName = `UserImages/`+myFile;
      let fileUpload = bucket.file(newFileName);
      const blobStream = fileUpload.createWriteStream({
        metadata: {
          contentType: file.mimetype
        }
      });
      blobStream.once('error', (error) => {
        reject('Something is wrong! Unable to upload at the moment.');
      });
      blobStream.once('finish', () => {
        // The public URL can be used to directly access the file via HTTP.
         const url = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/UserImages%2F${myFile}?alt=media`;
        resolve(url);
      });
  
      blobStream.end(file.buffer);
    });
    return prom;
  }  

router.get('/list',function (req,resp) {
    // console.log;  
    // Attach an asynchronous callback to read the data at our posts reference
    ref.once("value", function(snapshot) {
        var result= snapshot.val();
        var output=[];
        if(result!=null){
        Object.keys(result).forEach((elem)=>{
            var catNameAr=result[elem].categoryNameAr;
            var catNameEn=result[elem].categoryNameEn;
            var image=result[elem].image;
            output.push({
                "_id":elem,
                "categoryNameAr":catNameAr,
                "categoryNameEn":catNameEn,
                "image":image
            });
        })
    }
        return resp.render('categories/list',{data:output,msg:req.flash('msg')});     
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
    
});
router.get('/delete/:id',function (req,resp) {
    ref.child(req.params.id).remove();
    return resp.redirect('/category/list');

  });
router.get('/edit/:id',function (req,resp) {
    // console.log(req.params.id);
    ref.child(req.params.id).once("value",(snapshot)=>{
        var result= snapshot.val();
        console.log(result);
        
        var output={
            "_id":req.params.id,
            "categoryNameAr":result.categoryNameAr,
            "categoryNameEn":result.categoryNameEn,
            "image":result.image
        }
        return resp.render('categories/edit',{categoryObj:output});
    });

});

router.post('/edit',multer.single('catImg'),function (req,resp) {
    var categorynameAr=req.body.nameAr;
    var categorynameEn=req.body.nameEn;
    let file=req.file;

    var _id=req.body._id;
    if(categorynameAr==""||categorynameEn==""){
        req.flash("msg","Category Feilds Is Required");
        return resp.redirect('add');
    }else{
        if(file){
            uploadImageToStorage(file).then((success) => {
                ref.child(_id).set({
                    "categoryNameEn":categorynameEn,
                    "categoryNameAr":categorynameAr,
                    "image":success
                });        
                }).
                then((success)=>{
                    return resp.redirect('list');
    
                })
                .catch((error) => {
                  console.error(error);
                  return resp.redirect('/category/add');

                });
        }else{
            ref.child(_id).set({
                "categoryNameAr":categorynameAr,
                "categoryNameEn":categorynameEn
            });
            return resp.redirect('list');
        }
        
    }
});
module.exports = router;