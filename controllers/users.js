var express=require('express');
const myApp = express();
var router=express.Router();
var bodyParser=require('body-parser');
var firebase = require('firebase-admin');
var bodyParserMid=bodyParser.urlencoded();
var fs=require('fs');
var db = firebase.database();
var ref= db.ref('Providers');
var changeRef= db.ref('changeProviderData');

var refUsers=db.ref('UserData');
const Multer = require('multer');
const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
      fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
    }
  });
var bucket = firebase.storage().bucket();
// var bucket = firebase.storage().bucket("UserImages");
// Create a root reference
router.get('/add',function (req,resp) {
    // Attach an asynchronous callback to read the data at our posts reference
    db.ref("categories").once("value", function(snapshot) {
        var result= snapshot.val();
        var output=[];
        Object.keys(result).forEach((elem)=>{
            var catName=result[elem].categoryNameAr;
            output.push({
                "_id":elem,
                "categoryName":catName
            });            
        })        
        resp.render('users/add',{categories:output, msg:req.flash("msg")});    
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });

            
});
// var CategoryModel=mongoose.model('posts');
router.post('/add',multer.single('photoUrl'),function (req,resp) {
    let file = req.file;
    console.log(req.body);
    if (!(req.body.email&&req.body.displayName&&req.body.password&&req.body.phoneNumber&&req.body.lat&&req.body.lon&&req.body.ssn&&req.body.gender&&req.body.category)) {
                req.flash("msg","هناك خطأ فى البيانات , او يوجد تكرار");
                return resp.redirect('add');
    }
    if(file){
        uploadImageToStorage(file).then((success) => {
            addNewProvider(req.body.email,req.body.displayName,req.body.password,req.body.phoneNumber,success,req.body.lat,req.body.lon,req.body.ssn,req.body.gender,req.body.category,resp,req)
        }).catch((error) => {
            console.error(error);
            });
    }else{
        var gender=req.body.gender;
        var myPhoto;
        if(gender=="male"){
            myPhoto="https://png.icons8.com/color/180/user-male-skin-type-4.png";
        }else{
            myPhoto="https://png.icons8.com/color/1600/user-female-skin-type-5.png";
        }
        addNewProvider(req.body.email,req.body.displayName,req.body.password,req.body.phoneNumber,myPhoto,req.body.lat,req.body.lon,req.body.ssn,req.body.gender,req.body.category,resp,req)

    }

    
});
/**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */
const uploadImageToStorage = (file) => {
    let prom = new Promise((resolve, reject) => {
      if (!file) {
        reject('No image file');
      }
      let myFile=`${file.originalname}_${Date.now()}`;
      let newFileName = `UserImages/`+myFile;
      let fileUpload = bucket.file(newFileName);
      const blobStream = fileUpload.createWriteStream({
        metadata: {
          contentType: file.mimetype
        }
      });
      blobStream.on('error', (error) => {
        reject('Something is wrong! Unable to upload at the moment.');
      });
      blobStream.on('finish', () => {
        // The public URL can be used to directly access the file via HTTP.
         const url = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/UserImages%2F${myFile}?alt=media`;
        resolve(url);
      });
  
      blobStream.end(file.buffer);
    });
    return prom;
  }  
  router.get('/list',function (req,resp) {
    var providers=[];
    ref.once("value", function(snapshot) {
        var result= snapshot.val();
        db.ref("categories").once("value", function(snapshot) {
            var resultCat= snapshot.val();
            var outputCat=[];
            var providers=[];
            if(resultCat!=null){
            Object.keys(resultCat).forEach((elem)=>{
                var catName=resultCat[elem].categoryNameAr;
                outputCat.push({
                    "_id":elem,
                    "categoryName":catName
                });            
            })
            // console.log(result);
            
            Object.keys(result).forEach((elem)=>{
                outputCat.forEach((elemCat)=>{
                    if(elemCat['_id']==elem){
                       //  result[elem].catName=elemCat['categoryName'];
                         for (const key in result[elem]) {
                             if (result[elem].hasOwnProperty(key)) {
                                 const element = result[elem][key];
                                 element.catName=elemCat['categoryName']; 
                                 element.uid=key;
                                 providers.push(element);
                             }
                         }
                    }
                })
            });
            console.log(providers);
        }
            return resp.render('users/list',{users:providers,msg:req.flash('msg'),succ:req.flash('succ')});
                }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });

        
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
});
router.get('/listUsers',function (req,resp) {
    refUsers.once("value", function(snapshot) {
        var result= snapshot.val();
        var output=[];
        Object.keys(result).forEach((elem)=>{
            var userName=result[elem].username;
            output.push({
                "uid":elem,
                "userName":userName
            });
        })
        console.log(output);
        
        return resp.render('users/listUsers',{users:output,msg:req.flash('msg'),succ:req.flash('succ')});
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
});
router.get('/delete/:id/:catId',function (req,resp) {
    var uid=req.params.id;
    var catId=req.params.catId;
    var providerFavRef= db.ref('providerFav');
    firebase.auth().deleteUser(uid)
    .then(function(userRecord) {
        ref.child(catId).child(uid).remove();
        providerFavRef.child(uid).remove();
        var providerFavInUsers= db.ref('userfav');    
        providerFavInUsers.once("value", function(snapshot) {
            var result= snapshot.val();
            var output=[];
            
            Object.keys(result).forEach((elem)=>{
                Object.keys(result[elem]).forEach((currentProvider)=>{
                    if(uid==currentProvider){
                        providerFavInUsers.child(elem).child(uid).remove();
                    }
                });
                
            });

        });
       
    }).then(()=>{
        var  backURL=req.header('Referer') || '/';
        // do your thang
        return resp.redirect(backURL);

    })
    .catch(function(error) {
        console.log("Error deleting user:", error);
    });
  });
  router.get('/deleteUser/:id',function (req,resp) {
    var uid=req.params.id;    
    var userFavRef= db.ref('userfav');
    firebase.auth().deleteUser(uid)
    .then(function(userRecord) {
        db.ref("UserData").child(uid).remove();
        userFavRef.child(uid).remove();
        var userFavInProviders= db.ref('providerFav');    
        userFavInProviders.once("value", function(snapshot) {
            var result= snapshot.val();
            var output=[];
            Object.keys(result).forEach((elem)=>{
                Object.keys(result[elem]).forEach((currentUser)=>{
                    if(uid==currentUser){
                        userFavInProviders.child(elem).child(uid).remove();
                    }
                });
            });
        });
    }).then(()=>{
        var  backURL=req.header('Referer') || '/';
        // do your thang
        return resp.redirect(backURL);

    })
    .catch(function(error) {
        db.ref("UserData").child(uid).remove();
        var userFavInProviders= db.ref('providerFav');    
        userFavInProviders.once("value", function(snapshot) {
            var result= snapshot.val();
            var output=[];
            Object.keys(result).forEach((elem)=>{
                Object.keys(result[elem]).forEach((currentUser)=>{
                    if(uid==currentUser){
                        userFavInProviders.child(elem).child(uid).remove();
                    }
                });
            });
        });
        var  backURL=req.header('Referer') || '/';
        // do your thang
        return resp.redirect(backURL);
    });
  });
router.get('/edit/:id/:catId',function (req,resp) {
    // console.log(req.params.id);
    var uid=req.params.id;
    changeRef.child(uid).set({});
    firebase.auth().getUser(uid)
    .then(function(userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log("Successfully fetched user data:", userRecord.toJSON().providerData);
        resp.render('users/edit',{userObj:userRecord.toJSON(),catId:req.params.catId});
    })
    .catch(function(error) {
        console.log("Error fetching user data:", error);
    });

});
router.post('/edit',bodyParserMid,function (req,resp) {
    var displayName=req.body.displayName;
    var phoneNumber=req.body.phoneNumber;
    var email=req.body.email;
    var uid=req.body.uid;
    var catId=req.body.catId;
    
    if(displayName==""){
        req.flash("msg","Name Feild Is Required");
        return resp.redirect('add');
    }else{
        firebase.auth().updateUser(uid, {
            displayName : displayName,
            phoneNumber:phoneNumber,
            email:email
          })
        .then(function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            // console.log("+++++++++>"+userRecord.toJSON());
            
            ref.child(catId).child(uid).update({
                displayName:displayName,
                phoneNumber:phoneNumber,
                email:email               
            }
            );
            
            return resp.redirect('list');
        })
        .catch(function(error) {
            console.log("Error updating user:", error);
        });
    }
});
function addNewProvider(email,displayName,password,phoneNumber,photoURL,lat,lon,ssn,gender,catId,resp,req) {
    firebase.auth().createUser({
        email: email,
        emailVerified: true,
        phoneNumber: "+962"+phoneNumber,
        password: password,
        displayName: displayName,
        photoURL:photoURL,
        disabled: false
    })
    .then(function(userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
         userRecord.uid;
         ref.child(catId).child(userRecord.uid).set({
            "email":userRecord.email,
            "phoneNumber":userRecord.phoneNumber,
            "displayName":userRecord.displayName,
            "photoURL":userRecord.photoURL,
            "lat":lat,
            "lon":lon,
            "ssn":ssn,
            "gender":gender,
            "catId":catId
        });
    }).then((res)=>{
        return resp.redirect('list');
    })
    .catch(function(error) {
        req.flash("msg",error["message"]);
        return resp.redirect('add');
    });
  }
module.exports = router;