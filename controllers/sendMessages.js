var express=require('express');
var router=express.Router();
var bodyParser=require('body-parser');
var bodyParserMid=bodyParser.urlencoded();
var multer=require('multer');
// var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var validator = require('validator');
var firebase = require('firebase-admin');
var db = firebase.database();
var FCM = require('fcm-node');
var serverKey = 'AAAAFCQQ1DE:APA91bHWvdbSg5evJpnqLaKHCRh3ZrBjhKLUbIiKzGWQLqzTzYZSsYgH6dtA8V4a-ApX6BT-H8u1HrZRiHak1E8qEypOkjfUmd7P5Xgc_Z5GAm4cfs_OOabVQA_qcFFuqBlfy0hVTyix'; //put your server key here;
var fcm = new FCM(serverKey);
var DEVICE_TOKEN="f-YEIOqw5RA:APA91bHeH851ch-aHuE-ouZZv8kQ4Xq0nHz94HHlfIzWOgTSL5rm3oZwTdpy6KtHVPvtzb6NJWgcCAWELIfNmjvHySsFg77KNwXjl00-aXI_Cm43y-7n0zeVnBswUT4GqKxvUp0VB9Wb";
// var message = {
//     to: DEVICE_TOKEN, // required fill with device token or topics
//       "data" : {
//         "myTitle":"Portugal vs. Denmark",
//         "myBody":"great match!"
//       }
// };

//callback style
// fcm.send(message, function(err, response){
//     if (err) {
//         console.log(err)
//         console.log("Something has gone wrong!");
//     } else {
//         console.log("Successfully sent with response: ", response);
//     }
// });    
router.get('/add/:id',function (req,resp) {
     // Attach an asynchronous callback to read the data at our posts reference
         
        resp.render('messages/add',{uid:req.params.id,msg:req.flash('msg')});     
});
router.get('/addUsers',function (req,resp) {
    // Attach an asynchronous callback to read the data at our posts reference
        
       resp.render('messages/addUsers',{msg:req.flash('msg')});     
});
router.get('/addProviders',function (req,resp) {
    // Attach an asynchronous callback to read the data at our posts reference
        
       resp.render('messages/addProviders',{msg:req.flash('msg')});     
});
router.post('/addUsers',bodyParserMid,function (req,resp) {
        var message = {
            "to": "/topics/users",
              "data" : {
                "myTitle":req.body.title,
                "myBody":req.body.message
              }
        };

        //callback style
        fcm.send(message, function(err, response){
            if (err) {
                console.log(err)
                req.flash("msg","خطأ ارسال الاشعار بنجاح");
                return resp.redirect('/users/list');
            } else {
                req.flash("succ","تمت ارسال الاشعار بنجاح");
                return resp.redirect('/users/list');
            }
        });

});
router.post('/addProviders',bodyParserMid,function (req,resp) {
        var message = {
            data: {
            myTitle: req.body.title,
            myBody: req.body.message
            },
            "to": "/topics/providers",
        };

        //callback style
        fcm.send(message, function(err, response){
            if (err) {
                console.log(err.message)
            } else {
                req.flash("succ","تمت ارسال الاشعار بنجاح");
                return resp.redirect('/users/list');
            }
        });
});
router.post('/add/:id',bodyParserMid,function (req,resp) {
    db.ref("Tokens").child(req.params.id).on("value", function(snapshot) {
        var token= snapshot.val();  
        var message = {
            to: token, // required fill with device token or topics
              "data" : {
                "myTitle":req.body.title,
                "myBody":req.body.message
              }
        };

        //callback style
        fcm.send(message, function(err, response){
            if (err) {
                console.log(err)
                console.log("Something has gone wrong!");
            } else {
                req.flash("succ","تمت ارسال الاشعار بنجاح");
                return resp.redirect('/users/list');
            }
        }); 
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
});
module.exports = router;
