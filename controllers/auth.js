var express=require('express');
var router=express.Router();
var bodyParser=require('body-parser');
var bodyParserMid=bodyParser.urlencoded();
var multer=require('multer');
// var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var validator = require('validator');
var serviceAccount = require("../serviceAccountKey.json");

var firebase = require('firebase-admin');
var firebase2=require('firebase');
var config = {
    apiKey: "AIzaSyBjhjHb7AG0eIUGekhm-joxB-vES_B92v4",
    authDomain: "handmade-e357b.firebaseapp.com",
    databaseURL: "https://handmade-e357b.firebaseio.com",
    projectId: "handmade-e357b",
    storageBucket: "handmade-e357b.appspot.com",
    messagingSenderId: "86504428593"
};
firebase2.initializeApp(config);
firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://handmade-e357b.firebaseio.com",
  storageBucket: "gs://handmade-e357b.appspot.com"
});
var auth=firebase2.auth();

var db = firebase.database();
router.get('/',function (req,resp) {
    if(req.session.useremail=="maam@m.com"){
        return resp.redirect('/users/list');
    }else{
        resp.render('auth/login',{
            msg:req.flash("msg")   
         });
    }
    
});
router.post('/login',bodyParserMid,function (req,resp) {
    errL=[];
    var useremail=req.body.email;
    var pass=req.body.password;
    if(useremail != "maam@m.com"){
        req.flash("msg","You don't have permission to enter this site..");
        return resp.redirect('/');
    }    
    auth.signInWithEmailAndPassword(useremail,pass).then(e=>{
        req.session.useremail = useremail
        return resp.redirect('/users/list');
        
    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        req.flash("msg",errorMessage);
        errorMessage="";
        return resp.redirect('/');
    });
    

});
router.get('/logout',function (req,resp) {
    req.session.destroy(function () { 
        resp.redirect('/');
     })
  })

module.exports = router;