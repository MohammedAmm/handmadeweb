//Cloud Functions Modules
const functions = require('firebase-functions');
//Firebase Admin SDK Modules (it will send the Notifications to the user)
const admin = require('firebase-admin');
//init Admin SDK
admin.initializeApp(functions.config().firebase);

exports.sendNotificationOnNewFollow = functions.database.ref('favourites/{providerId}/{userId}/').onWrite(event => {
    const providerId = event.params.providerId;
    const userId = event.params.userId;
  // If un-follow we exit the function.
  if (!event.data.exists()) {
    return;
  }

 
  // Get the list of device notification tokens.
  const getDeviceTokensPromise = admin.database().ref(`Tokens/${providerId}`).once('value');
  // Get the follower Info.
  const getFollowerInfo = admin.database().ref(`UserData/${userId}/`).once('value');
  
//Execute the Functions
  return Promise.all([getDeviceTokensPromise, getFollowerInfo]).then((results)=> {
    const tokensSnapshot = results[0];
    const followerSnapshot = results[1];
    // Check if there are any device tokens.
    if (!tokensSnapshot.hasChildren()) {
      return console.log('There are no notification tokens to send to.');
    }
    const followerName = followerSnapshot.val().userName;
    console.log('Follower Name is: ', followerName);
    // Notification details.
    const payload = {
      data: {
        title: 'you have a New Favourite',
        body: `${followerName} has added You to his favourites.`
      }
    };
    // Listing all tokens.
    const tokens = Object.keys(tokensSnapshot.val());

    // Send notifications to all tokens.
    return admin.messaging().sendToDevice(tokens, payload).then(function(response){
      // For each message check if there was an error.
      const tokensToRemove = [];
      response.results.forEach((result, index) => {
        const error = result.error;
        if (error) {
          console.error('Failure sending notification to', tokens[index], error);
          // Cleanup the tokens who are not registered anymore.
          if (error.code === 'messaging/invalid-registration-token' ||
            error.code === 'messaging/registration-token-not-registered') {
            tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
          }
        }
      });
     return Promise.all(tokensToRemove);
    });
  });
});
