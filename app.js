var express=require('express');
var server =express();
var authRoutes=require('./controllers/auth');
var sendRoutes=require('./controllers/sendMessages');
var usersRoutes=require('./controllers/users');
var categoryRoutes=require('./controllers/categories');
var mongoose=require('mongoose');
var session=require('express-session');
var flash=require('connect-flash');
server.use(session({
    secret:"@#%#$^$%",
    cookie:{maxAge:1000*60*60*24*7}
}));
server.use(express.static('public'))
server.use(flash());
server.use('/',authRoutes); 
var isAdmin=(req, res, next) =>{
    if(req.session.useremail=="maam@m.com"){
       next();
    }else{
        return res.redirect('/');
    }
  }
server.use('/category',[isAdmin],categoryRoutes); 
server.use('/users',[isAdmin],usersRoutes);
 server.use('/sendMessages',[isAdmin],sendRoutes);


// server.use('/products',productsRoutes);
// server.use('/orders',ordersRoutes);
// server.get('/customer', function(req, res){
//     res.sendFile(__dirname + '/test.html');
// });
server.set('view engine','ejs');
server.set('views','./views');

var httServer=server.listen(process.env.PORT || 3000,function () {console.log
("Starting at 9090 .....")  });
var io = require('socket.io').listen(httServer);
var firebase = require('firebase-admin');
var db=firebase.database();
var changeReg=db.ref('changeProviderData');
var providersReg=db.ref('Providers');
function sync() {
    io.on('connection', function (socket) {

        changeReg.on("value",function(data){        
        var results=data.val();
        Object.keys(results).forEach(function (elem) {
            providersReg.child(results[elem]).child(elem).once("value",function(result){
                //    if(elem!="a"){
                    var providerId=elem;
                        socket.emit('news', {providerId,result});  

                    // }
                });
          });     
    })
    changeReg.off();
    changeReg.on('child_added', function(data) {
        
        var categoryId=data.val();
        var providerId=data.key;
        providersReg.child(categoryId).child(providerId).once("value",function(result){
                // if(providerId!="a"){
                    console.log("added");
                    socket.emit('news', {providerId,result});  
                // }
        })     
      });
    //   changeReg.on('child_removed', function(data) {
    //     var x=data.val();
    //       console.log("removed"+x);
    //   });
    // changeReg.on("child_added",function(data){
    //     console.log("asdnasdmasmdn,masnd,masnd,asmdn,as,mdas,mdmnasd,mas,nd");
        
    //     var results=data.val();
    //     Object.keys(results).forEach(function (elem) {
    //         providersReg.child(results[elem]).child(elem).on("value",function(result){
    //             io.on('connection', function (socket) {
    //                if(elem!="a"){
    //                     socket.emit('news', {elem,result});  

    //                 }
    //             });
    //         })     
    //       });     
    // })
})     ;
}
sync();
